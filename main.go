package main

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
	"strings"
)

func main() {
	stdin := bufio.NewScanner(os.Stdin)

	urls := FindUrls(stdin)

	if err := stdin.Err(); err != nil {
		panic(err)
	}

	fmt.Println(strings.Join(urls, "\n"))
}

func FindUrls(input *bufio.Scanner) []string {
	var urls []string

	for input.Scan() {
		line := input.Text()
		for _, element := range strings.Split(line, " ") {
			if strings.Contains(element, "://") {
				if _, err := url.Parse(element); err == nil {
					urls = append(urls, element)
				}
			}
		}
	}

	return urls
}
