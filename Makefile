TARGET := urlparse

$(TARGET): *.go
	go build -o $(@)


.PHONY: test
test:
	go test -v -race
