#!/bin/bash

tmux bind-key "u" run -b "tmux capture-pane -J -p -S -999 | urlparse | fzf-tmux --no-preview --reverse --exit-0 | xargs xdg-open"