package main

import (
	"bufio"
	"strings"
	"testing"
)

var data = `
this is some test http://juji.si with a fre semi.com links in it, ftp://google.com just
to see if scanner https://bla.com?whiat&is&this can catch them
`
var input = bufio.NewScanner(strings.NewReader(data))

func BenchmarkFindUrls(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FindUrls(input)
	}
}

func TestFindUrls(t *testing.T) {
	urls := FindUrls(input)

	expected := []string{
		"http://juji.si",
		"ftp://google.com",
		"https://bla.com?whiat&is&this",
	}

	if len(expected) != len(urls) {
		t.Fatalf("Expected %d urls, got %d", len(expected), len(urls))
	}

	for _, url := range expected {
		if !stringContains(urls, url) {
			t.Fatalf("Url '%s' should be found.", url)
		}
	}
}

func stringContains(elements []string, item string) bool {
	for _, element := range elements {
		if element == item {
			return true
		}
	}

	return false
}

func TestStringContains(t *testing.T) {
	elements := []string{
		"a", "b", "c",
	}

	if stringContains(elements, "nope") {
		t.Fatalf("Found nonexisting element")
	}

	for _, element := range elements {
		if !stringContains(elements, element) {
			t.Fatalf("Element '%s' should be in the list '%v'", element, elements)
		}
	}
}
